class Comment < ActiveRecord::Base
	belongs_to :post, :counter_cache => true
	belongs_to :user
# 	attr_accessor :comment_text, :resource, :number, :parent_comment_number, :post
	scope :noprivates, -> { joins(:posts).where("posts.hidden = ?", false) }
	has_and_belongs_to_many :recommending_users, -> { distinct }, :class_name => "User", :join_table => :comment_recommendations
	has_many :comment_recommendations
	
	has_one :parent_post_recommendation, :class_name => "PostRecommendation", :foreign_key => :recommending_comment_id
	has_one :parent_comment_recommendation, :class_name => "CommentRecommendation", :foreign_key => :recommending_comment_id

	belongs_to :parent_comment_user, :class_name => "User", :foreign_key => "parent_comment_user_id"
	
	has_and_belongs_to_many :ackd_users,  -> { distinct }, :class_name => "User", :join_table => :comment_acknowledgements
	
	def cmtid_text
		"#{self.post.postid_text}/#{self.number}"
	end
	
	def self.by_cmtid(s)
		if s =~ /^([a-z]+)\/(\d+)$/
			pst = Post.by_postid($1) or return nil
			return pst.comments.where(:number => $2.to_i).first
		else
			return nil
		end
	end
	
	def sorted_recommendations
		recs = []
		silents = []
		self.comment_recommendations.each do |c_rec|
			if c_rec.recommending_comment
				recs << [c_rec.user, c_rec.recommending_comment]
			else
				silents << c_rec.user
			end
		end
		[recs, silents]
	end
	
	def hidden_by?(some_user)
		self.user.hidden_by? some_user
	end
	
	def accessible_by?(some_user)
		not self.post.private? or self.post.user == some_user or self.post.target_user == some_user
	end
	
	def self.search(terms)
		dictionary = 'english'
		if terms =~ /[а-яА-Я]/i
			dictionary = 'russian'
		end
		r = self.select("setweight(to_tsvector('#{sanitize_sql dictionary}', coalesce(comment_text, '')), 'A') <=> plainto_tsquery('#{sanitize_sql dictionary}', '#{sanitize_sql terms}') AS rank, comments.*")
		return r.where("setweight(to_tsvector(:dictionary, coalesce(comment_text, '')), 'A') @@ plainto_tsquery(:dictionary, :terms)", dictionary: dictionary, terms: terms)
	end
end

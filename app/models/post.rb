class Post < ActiveRecord::Base
	scope :noprivates, -> { where(:private => false) }
	
	belongs_to :user
	has_and_belongs_to_many :tags,  -> { distinct }, :join_table => :posts_tags
	has_many :comments, -> { distinct.order("number ASC") }, :dependent => :destroy
# 	attr_accessor :resource, :post_content, :postid, :comment_counter, :target_user, :is_backup
	has_and_belongs_to_many :subscribers, -> { distinct }, :class_name => "User", :join_table => :post_subscriptions
	has_many :post_recommendations, :dependent => :destroy
	has_and_belongs_to_many :recommending_users, -> { distinct }, :class_name => "User", :join_table => :post_recommendations
	has_and_belongs_to_many :ackd_users,  -> { distinct }, :class_name => "User", :join_table => :post_acknowledgements
	belongs_to :target_user, :class_name => "User"

  before_create :assign_postid

  def self.next_postid
	postid = nil
	tentative_number = ActiveRecord::Base.connection.execute("SELECT reltuples AS approximate_row_count FROM pg_class WHERE relname = 'posts'").first.first.last.to_i
	begin
		postid = rand(tentative_number * 10 + 100)
	end while Post.where(postid: postid).first
	# Ok, this should be in transaction, but c'mon, what's the chance. But put this in transaction.
	postid
  end
  
	def assign_postid
		return if self.postid
		self.postid = Post.next_postid
	end
  
	def postid_text
		Huick.int_to_postid(postid)
	end
	
	def self.by_postid(which_postid)
		self.where(:postid => Huick.postid_to_int(which_postid)).first
	end
	
	def add_tag(tag_text)
		tag = Tag.find_or_create_by(:tag_text => tag_text)
		unless self.tags.exists? tag.id
			self.tags << tag
		end
	end
	
	def del_tag(tag_text)
		tag = Tag.find_or_create_by(:tag_text => tag_text)
		self.tags.delete tag
	end
	
	def sorted_recommendations
		recs = []
		silents = []
		self.post_recommendations.each do |c_rec|
			if c_rec.recommending_comment
				recs << [c_rec.user, c_rec.recommending_comment]
			else
				silents << c_rec.user
			end
		end
		[recs, silents]
	end
	
	def hidden_by?(some_user)
		return false unless some_user
		self.user.hidden_by? some_user or self.tags.map{|tag| tag.hidden_by?(some_user) }.any?
	end
	
	def accessible_by?(some_user)
		!self.private? or self.user == some_user or self.target_user == some_user
	end
	
	def self.search(terms)
		dictionary = 'english'
		if terms =~ /[а-яА-Я]/i
			dictionary = 'russian'
		end
		r = self.select("setweight(to_tsvector('#{sanitize_sql dictionary}', coalesce(post_content, '')), 'A') <=> plainto_tsquery('#{sanitize_sql dictionary}', '#{sanitize_sql terms}') AS rank, posts.*")
		return r.where("setweight(to_tsvector(:dictionary, coalesce(post_content, '')), 'A') @@ plainto_tsquery(:dictionary, :terms)", dictionary: dictionary, terms: terms)
	end
end

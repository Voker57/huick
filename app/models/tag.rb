class Tag < ActiveRecord::Base
# 	attr_accessor :tag_text
	has_and_belongs_to_many :subscribers,  -> { distinct }, :class_name => "User", :join_table => :tag_subscriptions
	
	has_and_belongs_to_many :hiders,  -> { distinct }, :class_name => "User", :join_table => :tag_hidings
	
	has_and_belongs_to_many :posts,  -> { distinct }, :join_table => :posts_tags
	
	before_save :downcase_text

	def downcase_text
		self.tag_text = Unicode::downcase self.tag_text
	end
	
	def self.from_text(tag_text)
		tag_existing = Tag.where(:tag_text => tag_text).first
		if tag_existing
			tag_existing
		else
			tag = Tag.new
			tag.tag_text = tag_text
			tag.save
			tag
		end
	end
	
	def hidden_by?(some_user)
		self.hiders.exists? some_user.id
	end
end

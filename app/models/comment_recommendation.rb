class CommentRecommendation < ActiveRecord::Base
	belongs_to :comment
	belongs_to :user
	
	belongs_to :recommending_comment, :class_name => "Comment", :dependent => :destroy, :foreign_key => :recommending_comment_id
end

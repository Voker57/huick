class UserSubscription < ActiveRecord::Base
# 	attr_accessor :notified
	belongs_to :subscriber, :class_name => "User", :foreign_key => :subscriber_id
	belongs_to :subscribed, :class_name => "User", :foreign_key => :subscribed_id
end

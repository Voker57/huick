class User < ActiveRecord::Base
# 	attr_accessor :jid, :username, :full_name, :description, :is_backup
	
	validates :username, :format => {with: /\A[a-zA-Z0-9\-_]*\z/}, :uniqueness => true
	
	has_many :posts, :dependent => :destroy
	has_many :comments, :dependent => :destroy
	has_many :post_recommendations
	has_many :comment_recommendations

  before_create :generate_access_token
  
	has_and_belongs_to_many :subscribers,  -> { distinct }, :class_name => "User", :join_table => :user_subscriptions, :foreign_key => :subscribed_id, :association_foreign_key => :subscriber_id
	has_and_belongs_to_many :subscribed_users,  -> { distinct }, :class_name => "User", :join_table => :user_subscriptions, :foreign_key => :subscriber_id, :association_foreign_key => :subscribed_id
	
	has_many :user_subscriptions, :foreign_key => :subscribed_id
	
	has_and_belongs_to_many :subscribed_posts,  -> { distinct }, :class_name => "Post", :join_table => :post_subscriptions
	
	has_and_belongs_to_many :subscribed_tags,  -> { distinct }, :class_name => "Tag", :join_table => :tag_subscriptions
	
	has_and_belongs_to_many :blacklisted_users,  -> { distinct }, :class_name => "User", :join_table => :user_blacklists, :foreign_key => :blacklisted_id, :association_foreign_key => :blacklister_id
	
	has_and_belongs_to_many :blacklisters,  -> { distinct }, :class_name => "User", :join_table => :user_blacklists, :foreign_key => :blacklister_id, :association_foreign_key => :blacklisted_id
	
	has_and_belongs_to_many :hidden_users,  -> { distinct }, :class_name => "User", :join_table => :user_hidings, :foreign_key => :hiding_user_id, :association_foreign_key => :hidden_user_id
	
	has_and_belongs_to_many :hiders,  -> { distinct }, :class_name => "User", :join_table => :user_hidings, :foreign_key => :hidden_user_id, :association_foreign_key => :hiding_user_id
	
	has_and_belongs_to_many :hidden_tags, -> { distinct }, :class_name => "Tag", :join_table => :tag_hidings
	
	def self.by_jid(which_jid)
		self.where(:jid => which_jid).first
	end
	
	def hidden_by?(other_user)
		return false if !other_user
		self.hiders.exists? other_user.id
	end
  
  def generate_access_token
    begin
      self.access_token = Digest::SHA512.hexdigest(open("/dev/urandom").read(1024))
    end while User.where(access_token: self.access_token).first
  end
end

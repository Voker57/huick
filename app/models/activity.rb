class Activity < ActiveRecord::Base
	belongs_to :post
	belongs_to :comment_recommendation
	belongs_to :post_recommendation
# 	attr_accessor :post_id, :comment_recommendation_id, :post_recommendation_id, :timestamp
	
	def self.cache_activities
		# Terrible, terrible magick
		current_post_id = Activity.where("post_id IS NOT NULL").order("id DESC").limit(1).pluck(:post_id)[0]
		current_comment_rec_id = Activity.where("comment_recommendation_id IS NOT NULL").order("id DESC").limit(1).pluck(:comment_recommendation_id)[0]
		current_post_rec_id = Activity.where("post_recommendation_id IS NOT NULL").order("id DESC").limit(1).pluck(:post_recommendation_id)[0]
		
		
			Post.where("id > ?", current_post_id || 0).find_in_batches() do |as|
			Activity.transaction do
				as.each do |post|
				
					Activity.new(:post_id => post.id, :timestamp => post.created_at).save!
				end
			end
			end
			
			PostRecommendation.where("id > ?", current_post_rec_id || 0).find_in_batches() do |as|
			Activity.transaction do
				as.each do |post_rec|
					Activity.new(:post_recommendation_id => post_rec.id, :timestamp => post_rec.created_at).save!
				end
			end
			end
			
			CommentRecommendation.where("id > ?", current_comment_rec_id || 0).find_in_batches() do |as|
			Activity.transaction do
				as.each do |cmt_rec|
					Activity.new(:comment_recommendation_id => cmt_rec.id, :timestamp => cmt_rec.created_at).save!
				end
			end
			end
	end
end

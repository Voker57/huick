class PostRecommendation < ActiveRecord::Base
	belongs_to :post
	belongs_to :user
# 	attr_accessor :text
	
	belongs_to :recommending_comment, :class_name => "Comment", :dependent => :destroy, :foreign_key => :recommending_comment_id
end

class GlobalSetting < ActiveRecord::Base
# 	attr_accessor :key, :value
	
	def self.[](k)
		s = self.first(:conditions => {:key => k})
		if s
			s.value
		else
			""
		end
	end
	
	def self.[]=(k,v)
		s = self.first(:conditions => {:key => k}) || self.new(:key => k)
		s.value = v
		s.save!
	end
end

module ApplicationHelper
	
	def postid_path(postid, options = {})
		url_for({controller: "posts", action: "post", postid: postid}.merge(options))
	end
	
	def post_path(post, options = {})
		postid_path(post.postid_text, options)
	end
	
	def comment_path(comment, options = {})
		commentid_path(comment.post.postid_text, comment.number, options)
	end
	
	def commentid_path(postid, number, options = {})
		url_for({controller: "posts", action: "post", postid: postid}.merge(options)) + "##{number}"
	end
	
	def user_path(user, options = {})
		url_for({controller: "users", action: "user", username: user.username}.merge(options))
	end
	
	def tag_path(tag, user = nil, options = {})
		if user
			url_for({controller: "tags", action: "posts", tag_text: tag.tag_text, username: user.username}.merge(options))
		else
			url_for({controller: "tags", action: "posts", tag_text: tag.tag_text}.merge(options))
		end
	end
	
	def unhide_path(url, options = {})
		url_for({controller: "application", action: "unhide", url: url}.merge(options))
	end
	
	def mark_it_up(string, options = {})
		path_options = {only_path: !options[:full_links]}
		string = ERB::Util.html_escape string
		string.gsub!(/(?<=\W|^)#([a-z]+(\/[0-9]+)?)(?=\W|$)/) do |s| s =~ /(?=\W|^)#([a-z]+(\/[0-9]+)?)(?=\W|$)/
			if $2
				wannabe_cmtid = $1
				pstid, cmtn = wannabe_cmtid.scan(/^([a-z]+)\/([0-9]+)/)[0]
				"<a href='#{commentid_path(pstid, cmtn, path_options)}'>##{wannabe_cmtid}</a>"
			else
				wannabe_postid = $1
				"<a href='#{postid_path(wannabe_postid, path_options)}'>##{wannabe_postid}</a>"
			end
		end
		string.gsub!(/(?<=\W|^)@([a-zA-Z0-9\-\_]+)(?=\W|$)/) do |s| s =~ /(?=\W|^)@([a-zA-Z0-9\-\_]+)(?=\W|$)/
			wannabe_username = $1
			target_user = User.new(username: wannabe_username)
			if target_user
				"<a href='#{user_path(target_user, path_options)}'>@#{wannabe_username}</a>"
			else
				s
			end
		end
		string.gsub!(/(?<=\W|^)([a-z][a-z0-9\-]+:[[[:graph:]]\-\._~:\/\?#%\[\]@!\$&'\(\)\*\+,;=\$]+)(?=\W|$)/, "<a href='\\1'>\\1</a>")
		string.gsub!("\n", "<br />")
		string.html_safe
	end
	
	def tags_to_sizes(tags)
		min_size = 10
		max_size = 57
		
		tags_freqs = {}
		tags.each do |tag|
			tags_freqs[tag["tag_text"]] = tag['count']
		end
		
		max_freq = 0
		min_freq = 0
		
		tags_freqs.each do |k,v|
			max_freq = v if v > max_freq
			min_freq = v if v < min_freq or min_freq == 0
		end
		
		tags_sizes = []
		
		tags_freqs.each do |k,v|
			tags_sizes << [k, min_size + (max_size - min_size).to_f / max_freq.to_f * v.to_f]
		end
		
		return tags_sizes
	end
end

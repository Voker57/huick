xml.instruct! :xml, :version => "1.0" 
xml.feed xmlns: "http://www.w3.org/2005/Atom" do
	if @user
		xml.title "Huick: *#{@tag.tag_text} @#{@user.username}"
		xml.subtitle "Recent posts from Huick tagged #{@tag.tag_text} by user @#{@user.username}"
		xml.link href: tag_path(@tag, @user, only_path: false)
		xml.link rel: "self", href: tag_path(@tag, only_path: false, format: :atom, username: @user.username)
		xml.id root_url + tag_path(@tag, only_path: false, format: :atom, username: @user.username)
	else
		xml.title "Huick: *#{@tag.tag_text}"
		xml.subtitle "Recent posts from Huick tagged #{@tag.tag_text}"
		xml.link href: tag_path(@tag, nil, only_path: false)
		xml.link rel: "self", href: tag_path(@tag, nil, only_path: false, format: :atom)
		xml.id tag_path(@tag, nil, only_path: false, format: :atom)
	end
	xml.updated @posts.first.created_at.iso8601 if @posts.first
	
	@posts.each do |post|
	xml.entry do
		xml.title "@#{post.user.username} ##{post.postid_text} -- #{truncate(post.post_content, length: 25)}"
		xml.link href: post_path(post, only_path: false)
		xml.id post_path(post, only_path: false)
		xml.updated post.created_at.iso8601
		
		tags = []
		post.tags.each do |p_tag|
			tags << "#{link_to p_tag.tag_text, tag_path(p_tag, post.user, only_path: false)}"
		end
		
		xml.content type: "html" do
			xml.cdata! "<div>" + tags.join(", ") + "</div><p>#{mark_it_up(post.post_content, full_links: true)}</p>"
		end
		xml.author do
			xml.name("@" + post.user.username)
		end
	end
	end
end
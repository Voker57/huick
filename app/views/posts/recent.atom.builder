xml.instruct! :xml, :version => "1.0" 
xml.feed xmlns: "http://www.w3.org/2005/Atom" do
	xml.title "Huick: recent"
	xml.subtitle "Recent posts from Huick"
	xml.link href: root_path(only_path: false)
	xml.link rel: "self", href: url_for(controller: :posts, action: :recent, only_path: false, format: :atom)
	xml.id url_for(controller: :posts, action: :recent, only_path: false, format: :atom)
	xml.updated @posts.first.created_at.iso8601 if @posts.first
	
	@posts.each do |post|
	xml.entry do
		xml.title "@#{post.user.username} ##{post.postid_text} -- #{truncate(post.post_content, length: 25)}"
		xml.link post_path(post, only_path: false)
		xml.id post_path(post, only_path: false)
		xml.updated post.created_at.iso8601
		
		tags = []
		post.tags.each do |p_tag|
			tags << "#{link_to p_tag.tag_text, tag_path(p_tag, post.user, only_path: false)}"
		end
		
		xml.content type: "html" do
			xml.cdata! "<div>" + tags.join(", ") + "</div><p>#{mark_it_up(post.post_content, full_links: true)}</p>"
		end
		xml.author do
			xml.name("@" + post.user.username)
		end
	end
	end
end
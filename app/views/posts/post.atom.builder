xml.instruct! :xml, :version => "1.0" 
xml.feed xmlns: "http://www.w3.org/2005/Atom" do
	xml.title "@#{@post.user.username} ##{@post.postid_text} -- #{truncate(@post.post_content, length: 25)}"
	xml.subtitle "Comments for ##{@post.postid_text} by @#{@post.user.username}: #{truncate(@post.post_content, length: 25)}"
	xml.link href: post_path(@post, only_path: false)
	xml.link rel: "self", href: post_path(@post, only_path: false, format: :atom)
	xml.id post_path(@post, only_path: false, format: :atom)
	xml.updated @post.comments.last.created_at.iso8601 if @post.comments.last
	
	@post.comments.last(25).each do |comment|
	xml.entry do
		xml.title "##{comment.cmtid_text}: @#{comment.user.username} #{ if comment.parent_comment_user then " -> @#{comment.parent_comment_user.username}" else "" end}#{ if comment.parent_post_recommendation then " recommended the post" elsif comment.parent_comment_recommendation then " recommended" else "" end }#{if comment.parent_comment_number > 0 then " /#{comment.parent_comment_number}" else "" end}"
		xml.link href: comment_path(comment, only_path: false)
		xml.id comment_path(comment, only_path: false)
		xml.updated comment.created_at.iso8601
		
		xml.content type: "html" do
			xml.cdata! "<p>#{mark_it_up(comment.comment_text, full_links: true)}</p>"
		end
		xml.author do
			xml.name("@" + comment.user.username)
		end
	end
	end
end
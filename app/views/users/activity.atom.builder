xml.instruct! :xml, :version => "1.0" 
xml.feed xmlns: "http://www.w3.org/2005/Atom" do
	xml.title @feed_title
	xml.subtitle @feed_subtitle
	xml.link href: url_for(only_path: false)
	xml.link rel: "self", href: url_for(format: :atom, only_path: false)
	xml.id url_for(format: :atom, only_path: false)
	xml.updated @activities.first.timestamp.iso8601 if @activities.first
	
	@activities.each do |activity|
	xml.entry do
		xml.id "activity.#{activity.id}"
		xml.updated activity.timestamp.iso8601
		if activity.post
			post = activity.post
			xml.title "@#{post.user.username} added ##{post.postid_text}: #{truncate(post.post_content, length: 25)}"
			xml.link href: post_path(post, only_path: false)
			
			tags = []
			post.tags.each do |p_tag|
				tags << "#{link_to p_tag.tag_text, tag_path(p_tag, post.user, only_path: false)}"
			end
			
			xml.content type: "html" do
				xml.cdata! "<div>" + tags.join(", ") + "</div><p>#{mark_it_up(post.post_content, full_links: true)}</p>"
			end
			xml.author do
				xml.name("@" + post.user.username)
			end
			
		elsif activity.comment_recommendation
			comment_recommendation = activity.comment_recommendation
			comment = activity.comment_recommendation.comment
			
			text = if comment_recommendation.recommending_comment
				" (#{comment_recommendation.recommending_comment.comment_text})"
			else
				nil
			end
			
			xml.title "@#{comment_recommendation.user.username} recommended ##{comment.cmtid_text}#{text}: #{truncate(comment.comment_text, length: 25)}"
			xml.link href: comment_path(comment, only_path: false)
			xml.updated comment_recommendation.created_at.iso8601
			
			xml.content type: "html" do
				xml.cdata! "<p>#{mark_it_up(comment_recommendation.comment.comment_text, full_links: true)}</p>"
			end
			xml.author do
				xml.name("@" + comment_recommendation.comment.user.username)
			end
		elsif activity.post_recommendation
			post_recommendation = activity.post_recommendation
			
			post = post_recommendation.post
			text = if post_recommendation.recommending_comment
				" (#{post_recommendation.recommending_comment.comment_text})"
			else
				nil
			end
			
			xml.title "@#{post_recommendation.user.username} recommended ##{post.postid_text}#{text}: #{truncate(post.post_content, length: 25)}"
			xml.link href: post_path(post, only_path: false)
			tags = []
			post.tags.each do |p_tag|
				tags << "#{link_to p_tag.tag_text, tag_path(p_tag, post.user, only_path: false)}"
			end
			
			xml.content type: "html" do
				xml.cdata! "<div>" + tags.join(", ") + "</div><p>#{mark_it_up(post.post_content, full_links: true)}</p>"
			end
			xml.author do
				xml.name("@" + post.user.username)
			end
 		end
	end
	end
end
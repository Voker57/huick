class SessionsController < ApplicationController
	def login
		session[:access_token] = params[:access_token]
		flash[:notice] = "Logged in!"
		if params[:redirect_to]
			redirect_to params[:redirect_to]
		else
			redirect_to "/"
		end
	end
end

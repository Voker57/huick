class ApplicationController < ActionController::Base
	protect_from_forgery
	
	before_action :log_in
	before_action :unhide_filter
	
	def unhide
		session[:unhide] = true
		redirect_to(params[:url])
	end
	
	def selfvalidate
		@registration_request = RegistrationRequest.where(:jid => params[:jid]).first or raise ActionController::RoutingError.new('Not Found')
		if params[:validate]
			
			if verify_recaptcha
 				User.transaction do
				
				if @registration_request.username.size > 30
					@registration_request.destroy
					flash[:error] ="Please take a smaller username. Don't overcompensate."
					redirect_to '/'
					return
 				end
				if User.find_by(username: @registration_request.username)
					@registration_request.destroy
					flash[:error] = "Username already taken. Try again."
					redirect_to '/'
					return
				end
				user = User.new(jid: params[:jid], username: @registration_request.username)
				user.save!
				@registration_request.destroy
				redirect_to '/', notice: "User with JID #{params[:jid]} registered successfully! Now you can LOGIN, use other Jabber commands and rob corovans."
				end
			else
				flash[:error] = "ReCAPTCHA failed!"
			end
		end
	end
	
	def search
		if params[:search] && (params[:search].size > 50 || params[:search].size < 3)
			raise ActionController::RoutingError.new('Forbidden')
		end
		@search_terms = params[:search]
		@search_mode = params[:kind]
		
		case params[:kind]
		when "posts"
			sorted_posts = Post.search(params[:search].to_s).order("rank DESC")
			hidden_posts = if @unhide or not @current_user then
				sorted_posts
			else
				Huick.hide(sorted_posts, @current_user)
			end
			@posts = hidden_posts.page(params[:page])
		when "comments"
			sorted_comments = Comment.search(params[:search].to_s).order("rank DESC")
			hidden_comments = if @unhide or not @current_user then
				sorted_comments
			else
				Huick.hide(sorted_comments, @current_user)
			end
			@comments = hidden_comments.page(params[:page])
		end
	end
	
	private
	
	def log_in
		if session[:access_token].to_s != ""
			@current_user = User.where(:access_token => session[:access_token]).first
		end
	end
	
	def unhide_filter
		if session[:unhide]
			session[:unhide] = nil
			@unhide = true
		end
	end
	
	
end

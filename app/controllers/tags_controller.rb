class TagsController < ApplicationController
	include ApplicationHelper
	def posts
		@tag = Tag.where(:tag_text => params[:tag_text]).first
		raise ActionController::RoutingError.new('Not Found') unless @tag
		
		basic_posts = if params[:username]
			@user = User.where(:username => params[:username]).first
			@tag.posts.noprivates.where(:user_id => @user.id).order("created_at DESC").preload(:tags, :user)
		else
			@tag.posts.noprivates.order("created_at DESC").preload(:tags, :user)
		end
		
		hidden_posts = if @unhide or not @current_user then
			basic_posts 
		else
			Huick.hide(basic_posts, @current_user)
		end
		
		@posts = if request.format.atom?
			hidden_posts.limit(25)
		elsif params[:search]
			hidden_posts.search(params[:search]).page(params[:page])
		else
			hidden_posts.page(params[:page])
		end
		
		respond_to do |format|
			format.html
			format.atom
		end
	end
	
	def index
		if params[:username]
			@user = User.find(:username => params[:username])
			raise ActionController::RoutingError.new('Not Found') unless @user
			
			@tags_data = Tag.connection.select_all("SELECT tags.tag_text, count(tags.id) FROM tags INNER JOIN posts_tags ON posts_tags.tag_id = tags.id INNER JOIN posts ON posts_tags.post_id = posts.id INNER JOIN users on posts.user_id = users.id WHERE posts.user_id = #{@user.id} AND posts.private = 0 GROUP BY tags.id")
		else
			@tags_data = Tag.connection.select_all("SELECT tags.tag_text, count(tags.id) FROM tags INNER JOIN posts_tags ON posts_tags.tag_id = tags.id INNER JOIN posts ON posts_tags.post_id = posts.id WHERE posts.private = 0 GROUP BY tags.id")
		end
	end
end

class PostsController < ApplicationController
	include PostsHelper
	include ApplicationHelper
	include ActionView::Helpers::TextHelper
	def post
		@post = Post.where(:postid => Huick.postid_to_int(params[:postid])).preload(:post_recommendations, :user).first
		raise ActionController::RoutingError.new('Not Found') unless @post
		
		raise ActionController::RoutingError.new('Forbidden') unless @post.accessible_by? @current_user
		
		@comments = if @unhide or not @current_user then
			@post.comments
		else
			Huick.hide(@post.comments, @current_user)
		end.preload(:comment_recommendations, :user, :parent_comment_recommendation, :parent_post_recommendation, :parent_comment_user, :post)
		
		respond_to do |format|
			format.html
			format.atom
		end
	end
	
	def recent
		sorted_posts = Post.order("created_at DESC").noprivates.preload(:tags, :user)
		
		hidden_posts = if @unhide or not @current_user then
			sorted_posts 
		else
			Huick.hide(sorted_posts, @current_user)
		end
		
		if request.format.atom?
			@posts = hidden_posts.limit(25)
		else
			@posts = hidden_posts.page(params[:page])
		end
		

		respond_to do |format|
			format.html
			format.atom
		end
	end
	
	def pointy
		redirect_to(action: :post, postid: Huick.int_to_postid(Huick.pstoid_to_int(params[:postid])))
	end
end

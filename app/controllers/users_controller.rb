class UsersController < ApplicationController
	def posts
		@user = User.where(:username => params[:username]).first
		raise ActionController::RoutingError.new('Not Found') unless @user
		
		@feed_title = "Huick: Recent posts by @#{@user.username}"
		@feed_subtitle = "Recent posts by user #{@user.username} on Huick"
		
		unless not @unhide and @user.hidden_by? @current_user
			Activity.cache_activities
			
			basic_activities = Activity.joins("LEFT JOIN posts ON posts.id = activities.post_id").where("posts.user_id = ?", @user.id).where("posts.private != ? OR posts.private IS NULL", true).order("timestamp DESC").preload({:post => [:tags, :user]})
			
			hidden_activities = if not @unhide and @current_user
				basic_activities.where("NOT EXISTS (SELECT * FROM user_hidings WHERE posts.user_id = user_hidings.hidden_user_id AND user_hidings.hiding_user_id = ?)", @current_user.id)
			else
				basic_activities
			end
			
			@activities = hidden_activities.page(params[:page])
		end
		respond_to do |format|
			format.atom {render :action => :activity}
			format.html
		end
	end
	
	def recommendations
		@user = User.where(:username => params[:username]).first
		raise ActionController::RoutingError.new('Not Found') unless @user
		
		@feed_title = "Huick: Recent recommendations by @#{@user.username}"
		@feed_subtitle = "Recent recommendations by user #{@user.username} on Huick"
		
		unless not @unhide and @user.hidden_by? @current_user
			
			if params[:page].to_i <= 1
				@user = User.where(:username => params[:username]).first
				raise ActionController::RoutingError.new('Not Found') unless @user
				
				@tags = Tag.joins("LEFT JOIN posts_tags ON posts_tags.tag_id = tags.id LEFT JOIN posts ON posts_tags.post_id = posts.id LEFT JOIN users on posts.user_id = users.id").where("posts.user_id = ?", @user.id)
			end
			
			Activity.cache_activities
			
			basic_activities = Activity.joins("LEFT JOIN post_recommendations ON post_recommendations.id = activities.post_recommendation_id LEFT JOIN comment_recommendations ON comment_recommendations.id = activities.comment_recommendation_id").where("post_recommendations.user_id = ? OR comment_recommendations.user_id = ?", @user.id, @user.id).order("timestamp DESC").preload({:post_recommendation => [{:post => [:tags, :user]}, {:recommending_comment => [:post, :user]}, :user], :comment_recommendation => [{:comment => [:user]}, {:recommending_comment => [:post, :user]}, :user]})
			
			hidden_activities = if not @unhide and @current_user
				basic_activities.where("NOT EXISTS (SELECT * FROM user_hidings LEFT JOIN posts AS the_inner_posts ON the_inner_posts.user_id = user_hidings.hidden_user_id WHERE the_inner_posts.id = post_recommendations.post_id	AND user_hidings.hiding_user_id = ?)", @current_user.id).where("NOT EXISTS (SELECT * FROM user_hidings LEFT JOIN comments ON comments.user_id = user_hidings.hidden_user_id WHERE comments.id = comment_recommendations.comment_id AND user_hidings.hiding_user_id = ?)", @current_user.id)
			else
				basic_activities
			end
			
			@activities = hidden_activities.page(params[:page])
		end
		
		respond_to do |format|
			format.atom {render :action => :activity}
			format.html
		end
	end

	def user
		@user = User.where(:username => params[:username]).first
		raise ActionController::RoutingError.new('Not Found') unless @user
		@feed_title = "Huick: Recent activity by @#{@user.username}"
		@feed_subtitle = "Recent posts and recommendations by user #{@user.username} on Huick"
		unless not @unhide and @user.hidden_by? @current_user
			
			if params[:page].to_i <= 1
				@user = User.where(:username => params[:username]).first
				raise ActionController::RoutingError.new('Not Found') unless @user
				
				@tags_data = Tag.connection.select_all("SELECT tags.tag_text, count(tags.id) FROM tags INNER JOIN posts_tags ON posts_tags.tag_id = tags.id INNER JOIN posts ON posts_tags.post_id = posts.id INNER JOIN users on posts.user_id = users.id WHERE posts.user_id = #{@user.id} AND posts.private = false GROUP BY tags.id")
			end
			
			Activity.cache_activities
			
			basic_activities = Activity.joins("LEFT JOIN posts ON posts.id = activities.post_id LEFT JOIN post_recommendations ON post_recommendations.id = activities.post_recommendation_id LEFT JOIN comment_recommendations ON comment_recommendations.id = activities.comment_recommendation_id").where("posts.user_id = ? OR post_recommendations.user_id = ? OR comment_recommendations.user_id = ?", @user.id, @user.id, @user.id).where("posts.private != ? OR posts.private IS NULL", true).order("timestamp DESC").preload({:post => [:tags, :user], :post_recommendation => [{:post => [:tags, :user]}, {:recommending_comment => [:post, :user]}, :user], :comment_recommendation => [{:comment => [:user]}, {:recommending_comment => [:post, :user]}, :user]})
			
			hidden_activities = if not @unhide and @current_user
				basic_activities.where("NOT EXISTS (SELECT * FROM user_hidings WHERE posts.user_id = user_hidings.hidden_user_id AND user_hidings.hiding_user_id = ?)", @current_user.id).where("NOT EXISTS (SELECT * FROM user_hidings LEFT JOIN posts AS the_inner_posts ON the_inner_posts.user_id = user_hidings.hidden_user_id WHERE the_inner_posts.id = post_recommendations.post_id	 AND user_hidings.hiding_user_id = ?)", @current_user.id).where("NOT EXISTS (SELECT * FROM user_hidings LEFT JOIN comments ON comments.user_id = user_hidings.hidden_user_id WHERE comments.id = comment_recommendations.comment_id AND user_hidings.hiding_user_id = ?)", @current_user.id)
			else
				basic_activities
			end
			
			@activities = hidden_activities.page(params[:page])
		end
		
		respond_to do |format|
			format.atom {render :action => :activity}
			format.html
		end
	end
	
	def subscriptions
		@user = User.where(:username => params[:username]).preload(:subscribers, :subscribed_users).first
		raise ActionController::RoutingError.new('Not Found') unless @user
		
		@feed_title = "Huick: Subscriptions of @#{@user.username}"
		@feed_subtitle = "Recent posts and recommendations from subscriptions of user #{@user.username} on Huick"
		
		Activity.cache_activities
		
		basic_activities = Activity.joins("LEFT JOIN posts ON posts.id = activities.post_id LEFT JOIN post_recommendations ON post_recommendations.id = activities.post_recommendation_id LEFT JOIN comment_recommendations ON comment_recommendations.id = activities.comment_recommendation_id INNER JOIN users ON posts.user_id = users.id OR post_recommendations.user_id = users.id OR comment_recommendations.user_id = users.id INNER JOIN user_subscriptions ON user_subscriptions.subscribed_id = users.id").where("user_subscriptions.subscriber_id = ?", @user.id).where("posts.private != ? OR posts.private IS NULL", true).order("activities.timestamp DESC").preload({:post => [:tags, :user], :post_recommendation => [{:post => [:tags, :user]}, {:recommending_comment => [:post, :user]}, :user], :comment_recommendation => [{:comment => [:user]}, {:recommending_comment => [:post, :user]}, :user]})
		
		hidden_activities = if not @unhide and @current_user
			basic_activities.where("NOT EXISTS (SELECT * FROM user_hidings WHERE posts.user_id = user_hidings.hidden_user_id AND user_hidings.hiding_user_id = ?)", @current_user.id).where("NOT EXISTS (SELECT * FROM user_hidings JOIN posts AS the_inner_posts ON the_inner_posts.user_id = user_hidings.hidden_user_id WHERE the_inner_posts.id = post_recommendations.post_id	AND user_hidings.hiding_user_id = ?)", @current_user.id).where("NOT EXISTS (SELECT * FROM user_hidings JOIN comments ON comments.user_id = user_hidings.hidden_user_id WHERE comments.id = comment_recommendations.comment_id AND user_hidings.hiding_user_id = ?)", @current_user.id)
		else
			basic_activities
		end
		
		@activities = hidden_activities.page(params[:page])
		
		respond_to do |format|
			format.atom {render :action => :activity}
			format.html
		end
	end
end

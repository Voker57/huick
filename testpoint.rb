PSTO_PATH="/home/voker57/point"



def int_to_pstoid(int)
	int = int.to_i
	result = ""
	while int > 0
		result.prepend(((int % 27) + 'a'.ord - 1).chr)
		int = int / 27
	end
	result
	end
	
	def pstoid_to_int(postid)
		result = 0
		chars = postid.chars.to_a.reverse
		chars.each_index do |i|
			result += (chars[i].ord - 'a'.ord + 1) * (27 ** i)
		end
		result
	end
# 
# puts int_to_pstoid(pstoid_to_int("hkz"))
# exit

def fake_http_request(path)
	puts "Faking HTTP: #{path}"
	path = URI.unescape(path)
	path = path.gsub(/#.*$/,"")
	if File.file? "#{PSTO_PATH}/#{path}"
		Nokogiri.parse(File.read("#{PSTO_PATH}/#{path}"))
	elsif File.file? "#{PSTO_PATH}/#{path}/index.html"
		Nokogiri.parse(File.read("#{PSTO_PATH}/#{path}/index.html"))
	else
		puts "404"
		return nil
	end
end

def get_user(username)
	u_t = Translit.convert(username, :english).gsub(/[^a-zA-Z0-9\-]/,"")
# 	p username
# 	p u_t
	if u_t != username
		username = "uebok-po-imeni-#{u_t}"
	end
# 	p username
	u = User.find_or_create_by(:username => username.downcase)
end

def get_tag(tagtext)
	t = Tag.find_or_create_by(:tag_text => tagtext)
end

def unmark(s)
	HTMLEntities.new.decode(s.strip.gsub(/<a class="user" href=".*?">(.*?)<\/a>/, "@\\1").gsub(/<a href="(.*?)".*?<\/a>/,"\\1").gsub(/<br.*?>/,"\n").gsub(/<.*?>/,"").gsub(/(?<=\W|^)#([a-z]+(\/[0-9]+)?)(?=\W|$)/) do 
		str = "#"
		str << Huick.int_to_postid(Huick.pstoid_to_int($1))
		str << $2.to_s
		str
	end)
end

def warn(s)
	File.open("warnings.txt", "a") do |f| f.puts  s end
end

def scrape_post(link, post_h)
	post_page = fake_http_request(link)
	if !post_page
		warn("Link #{link} does not exist")
		return
	end
	if !post_page.at("div[@id='top-post']")
		warn("Link #{link} does not contain a post")
		return
	end
	psto_id = post_page.at("div[@id='top-post']")["data-id"]
# 	ActiveRecord::Base.transaction do
	post = Post.new
	post.postid = Huick.pstoid_to_int(psto_id)
	return if post_h[post.postid]
	post_h[post.postid] = true
	u = get_user(post_page.at("div[@id='subheader']/span[@class='active user']").inner_text.gsub(/^@/,"").strip		)
	post.user = u
	u.save!	
	post_date = DateTime.parse(post_page.at('div[@id="top-post"]//div[@class="post-created"]').inner_text.strip)

	post.resource = ""
	post.created_at = post_date
	post.updated_at = post.created_at
	post.notified = true
	post.post_content = unmark(post_page.at('div[@id="top-post"]//div[@class="text"]').inner_html)
	post_page.search('div[@id="top-post"]//div[@class="clearfix"]/a[@class="postimg"]').each do |img|
		post.post_content << "\n" << img["href"]
	end
	post_page.search('div[@id="top-post"]//div[@class="clearfix"]/a[@class="postimg youtube"]').each do |img|
		post.post_content << "\n" << img["href"]
	end
	post_page.search('div[@id="top-post"]//p[@class="post-tags"]/a[@class="post-tag"]').map {|tag_a| Unicode::downcase(tag_a.inner_text)}.uniq.each do |tag_text|
		post.tags << get_tag(tag_text)
	end
	usermap = {}
	max_number = 1
	post_page.search('div[@id="comments"]//article').each do |comment_div|
		comment = Comment.new
		comment.number = comment_div["data-comment-id"].to_i
		post.comment_counter += 1
		if post.comment_counter <= comment.number
			post.comment_counter = comment.number + 1
		end
		comment.user = if comment_div.at('header[@class="post-header"]/a[@class="post-author user"]')
			get_user(comment_div.at('header[@class="post-header"]/a[@class="post-author user"]').inner_text.strip)
			       else
				       get_user("anonymous")
			       end
		usermap[comment.number] = comment.user
		
		comment_date = DateTime.parse(comment_div.at("div[@class='post-created']").inner_text.strip)
		comment.resource = ""
		comment.created_at = comment_date
		comment.updated_at = comment.created_at
		comment.notified = true
		comment.comment_text = unmark(comment_div.at('div[@class="post-content"]/div[@class="text"]').inner_html)
		if cmt= comment_div.at('div[@class="post-content"]/div[@class="clearfix"]/div[@class="post-id"]/a[@class="comment-id"]')
			comment.parent_comment_number = cmt.inner_text.strip.gsub("/","")
			comment.parent_comment_user = usermap[comment.parent_comment_number]

		end
		post.comments << comment
	end
	post.save!
# 	end
end


post_h = {}
Dir.glob("#{PSTO_PATH}/*").each do |pth|
	ActiveRecord::Base.transaction do
	scrape = false
	next unless pth =~ /point\/(\S+).point.im/
	username = pth.scan(/point\/(\S+).point.im/)[0][0]
	scrape = true # if username == "arts"
	next unless scrape
	
	u = nil
	u_page = nil

	u = get_user(username)
	u_page = fake_http_request("#{username}.point.im")
	next unless u_page
	next unless u_page.at('div[@id="about"]')
	u.description = u_page.at('div[@id="about"]').inner_text.strip
	if u_page.at('div[@class="info"]/div[@class="name"]')
		u.full_name = u_page.at('div[@class="info"]/div[@class="name"]').inner_text.strip
	end
	subs_page = fake_http_request("#{username}.point.im/subscribers.html")
	subs_page.search('div[@id="content"]//div[@class="users"]/a[@class="user"]').each do |reader|
		next if UserSubscription.where("subscriber_id = ? AND subscribed_id = ?", get_user(reader.at('span[@class="user"]').inner_text.strip).id, u.id).first
		u.subscribers << get_user(reader.at('span[@class="user"]').inner_text.strip)
	end
# 	p u
	u.save!
	u.user_subscriptions.each do |u_s|
		u_s.notified = true
	end
	u.save!

# 	next
	page_n = 1
	begin
		u_page = if page_n == 1
				 fake_http_request("#{username}.point.im/index.html")
			 else
				 fake_http_request("#{username}.point.im/#{page_n}.html") 
			 end
		if u_page == nil
			warn("Not a user page: #{username}.point.im/#{page_n}.html")
			break
		end
		#p u_page
# 		puts "esketit"
		u_page.search('div[@id="content"]/div[@class="content-wrap"]/div[@class="post"]').each do |post_div|
# 		puts "postdiv"
# 		ActiveRecord::Base.transaction do
			if rec = post_div.at("div[@class='post-content']/div[@class='rec']")
				# it's a recommendation
# 				puts rec
				rec_id = post_div.at("div[@class='clearfix']/div[@class='post-id']").inner_text.strip
				reclink =  post_div.at("div[@class='clearfix']/div[@class='post-id']/a")["href"]
				time = DateTime.parse(post_div.at("time[@class='post-created']"))
				rtext = if post_div.at("div[@class='rtext']") then
						unmark(post_div.at("div[@class='rtext']").inner_html)
					else
						nil
					end
				if rec_id =~ /^#([a-z]+)$/
					scrape_post("#{username}.point.im/#{reclink}", post_h)
					pst = Post.find_by(postid: Huick.pstoid_to_int($1))
					if pst
						Huick.recommend_post(u, "", pst, rtext, suppress_nudge: true, backdate: time)
					end
				elsif rec_id =~ /^#([a-z]+)\/(\d+)$/
					scrape_post("#{username}.point.im/#{reclink}", post_h)
					pst = Post.find_by(postid: Huick.pstoid_to_int($1))
					if pst != nil
					cmt = pst.comments.find_by(number: $2.to_i)
					if cmt != nil
						Huick.recommend_comment(u, "", cmt , rtext, suppress_nudge: true, backdate: time)
					end
					end
				end
			else

				psto_id = post_div["data-id"]
				scrape_post("#{username}.point.im/#{psto_id}.html", post_h)
			end
		end
# 		end
		page_n += 1
	end while u_page.at('a[@id="more"]')
	u.save!
	end
# 	puts "TIME TO STOP THE PAYNE"
# 	sleep 5
end

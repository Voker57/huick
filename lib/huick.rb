# encoding: utf-8

require 'unicode'

module Huick
	class NoSuchPostError < Exception; end
	class NoSuchCommentError < Exception; end
	class NoSuchUserError < Exception; end
	class NotRegisteredError < Exception; end
	class AlreadySubscribedError < Exception; end
	class NoPermissionError < Exception; end
	class NotSubscribedError < Exception; end
	class AlreadyBlacklistedError < Exception; end
	class NotBlacklistedError < Exception; end
	class AlreadyHiddenError < Exception; end
	class NotHiddenError < Exception; end
	class SaemPersonError < Exception; end
	class UserBannedError < Exception; end
	class AlreadyRegisteredError < Exception; end
	class TooMuchError < Exception; end
	class PrivatePostError < Exception; end
	class UsernameTakenError < Exception; end
	class TooFastError < Exception; end
	class BadUsernameError < Exception; end
	
	COMBINATIONS = ["re", "ri", "ro", "ra", "te", "ti", "to", "ta", "pe", "pi", "po", "pa", "se", "si", "so", "sa", "de", "di", "do", "da", "fe", "fi", "fo", "fa", "ge", "gi", "go", "ga", "he", "hi", "ho", "ha", "ke", "ki", "ko", "ka", "le", "li", "lo", "la", "ze", "zi", "zo", "za", "xe", "xi", "xo", "xa", "ve", "vi", "vo", "va", "be", "bi", "bo", "ba", "ne", "ni", "no", "na", "me", "mi", "mo", "ma"]
	
	def self.int_to_pstoid(int)
		int = int.to_i
		result = ""
		while int > 0
			result.prepend(((int % 27) + 'a'.ord - 1).chr)
			int = int / 27
		end
		result
	end
	
	def self.pstoid_to_int(postid)
		result = 0
		chars = postid.chars.to_a.reverse
		chars.each_index do |i|
			result += (chars[i].ord - 'a'.ord + 1) * (27 ** i)
		end
		result
	end
	
	def self.int_to_postid(int)
		int = int.to_i
		result = ""
		while int > 0
			result << (COMBINATIONS[int % COMBINATIONS.size])
			int = int / COMBINATIONS.size
		end
		if result.empty?
			result = "re"
		end
		result
	end
	
	def self.postid_to_int(postid)
		result = 0
		chars = postid.chars.each_slice(2).to_a
		chars.each_index do |i|
			c = chars[i].join
			ind = COMBINATIONS.index c
			result += ind * (COMBINATIONS.count ** i)
		end
		result
	end
		
	def self.require_unbanned(user)
		if user and user.banned?
			raise UserBannedError
		end
	end
	
	def self.nudge_bot
		return unless Huick.bot_socket
		begin
		Socket.unix(Huick.bot_socket) do |sock|
			
		end
		
		rescue Exception => e
			# Uh, well, whatever
			if defined? Rails
				Rails.logger.error("Exception: #{e} when nudging bot")
			else
				puts("Exception: #{e} when nudging bot")
			end
		end
	end
	
	def self.delete_comment(user, comment)
		raise NoPermissionError unless comment.user == user or comment.post.user == user
		Huick.require_unbanned user
		comment.destroy
	end
	
	def self.delete_post(user, post)
		raise NoPermissionError unless post.user == user
		Huick.require_unbanned user
		post.destroy
	end
	
	def self.unrecommend_post(user, post)
		Huick.require_unbanned user
		post.recommending_users.delete user
		post.save!
	end
	
	def self.recommend_post(user, resource, post, text = nil, options = {})
		raise NoPermissionError if post.private
		Huick.require_unbanned user
		raise SaemPersonError if post.user == user
		p_rec = PostRecommendation.new
		p_rec.post = post
		p_rec.user = user
		if text
			p_rec.recommending_comment =	self.add_comment(user, resource, post, text, nil, :supress_nudge => true, backdate: options[:backdate])
		end
		p_rec.created_at = options[:backdate] if options[:backdate]
		p_rec.save!
		
		Huick.subscribe_to_post(user, post, :ensure => true)
		
		Huick.nudge_bot unless options[:supress_nudge]
		p_rec
	end
	
	def self.unrecommend_comment(user, comment)
		Huick.require_unbanned user
		comment.recommending_users.delete user
		comment.save!
	end
	
	def self.recommend_comment(user, resource, comment, text = nil, options = {})
		Huick.require_unbanned user
		raise SaemPersonError if comment.user == user
		c_rec = CommentRecommendation.new
		c_rec.comment = comment
		c_rec.user = user
		c_rec.created_at = options[:backdate] if options[:backdate]
		if text
			c_rec.recommending_comment = self.add_comment(user, resource, nil, text, comment, :supress_nudge => true, backdate: options[:backdate])
		end
		c_rec.save!
		
		Huick.subscribe_to_post(user, comment.post, :ensure => true)
		
		Huick.nudge_bot unless options[:supress_nudge]
		c_rec
	end
	
	def self.require_user(jid)
		User.by_jid(jid) or raise NotRegisteredError
	end
	
	def self.require_post(postid)
		Post.by_postid(postid) or raise NoSuchPostError
	end
	
	def self.require_comment(cmtid)
		Comment.by_cmtid(cmtid) or raise NoSuchCommentError
	end
	
	def self.add_comment(user, resource, post, comment_text, parent_comment = nil, options = {})
		Huick.require_unbanned user
		# eliminate ambiguity
		post = parent_comment.post if parent_comment
		newnumber = post.comment_counter
		raise NoPermissionError if post.user.blacklisted_users.exists? user.id
		raise NoPermissionError unless post.accessible_by? user
		cmt = Comment.new
		cmt.post = post
		cmt.user = user
		cmt.comment_text = comment_text
		cmt.resource = resource
		cmt.number = newnumber
		cmt.created_at = options[:backdate] if options[:backdate]
		if parent_comment
			cmt.parent_comment_number = parent_comment.number
			cmt.parent_comment_user = parent_comment.user
		else
			cmt.parent_comment_number = 0
		end
		cmt.save!
		
		Huick.acknowledge(cmt, user)
		
		post.comment_counter += 1
		post.save!
		
		Huick.subscribe_to_post(user, post, :ensure => true)
		
		Huick.nudge_bot unless options[:supress_nudge]
		cmt
	end
	
	def self.acknowledge(something, user, &block)
		if block
			if something.ackd_users.exists? user.id
				return nil
			else
				block.call
				self.acknowledge something, user
			end
		else
			something.ackd_users << user
			something.save!
		end
	end
	
	def self.add_post(user, resource, post_content, tags = [], target_user = nil, private_flag = false)
		Huick.require_unbanned user
		pst = Post.new
		pst.post_content = post_content
		pst.target_user = target_user
		pst.resource = resource
		pst.user = user
		tags.each do |tag|
			pst.add_tag tag
		end
		pst.private = private_flag
		pst.save!
		
		Huick.acknowledge(pst, user)
		
		Huick.subscribe_to_post(user, pst, :ensure => true)
		
		Huick.nudge_bot
		pst
	end
	
	def self.subscribe_to_post(user, post, options = {})
		Huick.require_unbanned user
		unless user.subscribed_posts.exists? post.id
			user.subscribed_posts << post
			user.save!
		else
			raise AlreadySubscribedError unless options[:ensure]
		end
	end
	
	def self.unsubscribe_from_post(user, post, options = {})
		Huick.require_unbanned user
		if user.subscribed_posts.exists? post.id
			user.subscribed_posts.delete(post)
			user.save!
		else
			raise NotSubscribedError unless options[:ensure]
		end
	end
	
	def self.subscribe_to_tag(user, tag, options = {})
		Huick.require_unbanned user
		unless user.subscribed_tags.exists? tag.id
			user.subscribed_tags << tag
			user.save!
		else
			raise AlreadySubscribedError unless options[:ensure]
		end
	end
	
	def self.unsubscribe_from_tag(user, tag, options = {})
		Huick.require_unbanned user
		if user.subscribed_tags.exists? tag.id
			user.subscribed_tags.delete(tag)
			user.save!
		else
			raise NotSubscribedError unless options[:ensure]
		end
	end
	
	def self.subscribe_to_user(user, target_user, options = {})
		Huick.require_unbanned user
		raise SaemPersonError if user == target_user
		unless target_user.subscribers.exists? user.id
			target_user.subscribers << user
			target_user.save!
		else
			raise AlreadySubscribedError unless options[:ensure]
		end
		Huick.nudge_bot
	end
	
	def self.unsubscribe_from_user(user, target_user)
		Huick.require_unbanned user
		if target_user.subscribers.exists? user.id
			target_user.subscribers.delete(user)
			target_user.save!
		else
			raise NotSubscribedError unless options[:ensure]
		end
	end
	
	def self.blacklist_user(user, target_user, options = {})
		Huick.require_unbanned user
		unless target_user.blacklisters.exists? user.id
			target_user.blacklisters << user
			target_user.save!
		else
			raise AlreadyBlacklistedError unless options[:ensure]
		end
	end
	
	def self.unblacklist_user(user, target_user, options = {})
		Huick.require_unbanned user
		if target_user.blacklisters.exists? user.id
			target_user.blacklisters.delete(user)
			target_user.save!
		else
			raise NotBlacklistedError unless options[:ensure]
		end
	end
	
	def self.hide_user(user, target_user, options = {})
		Huick.require_unbanned user
		unless target_user.hiders.exists? user.id
			target_user.hiders << user
			target_user.save!
		else
			raise AlreadyHiddenError unless options[:ensure]
		end
	end
	
	def self.unhide_user(user, target_user, options = {})
		Huick.require_unbanned user
		if target_user.hiders.exists? user.id
			target_user.hiders.delete(user)
			target_user.save!
		else
			raise NotHiddenError unless options[:ensure]
		end
	end
	
	def self.hide_tag(user, tag, options = {})
		Huick.require_unbanned user
		unless user.hidden_tags.exists? tag.id
			user.hidden_tags << tag
			user.save!
		else
			raise AlreadyHiddenError unless options[:ensure]
		end
	end
	
	def self.unhide_tag(user, tag, options = {})
		Huick.require_unbanned user
		if user.hidden_tags.exists? tag.id
			user.hidden_tags.delete tag
			user.save!
		else
			raise NotHiddenError unless options[:ensure]
		end
	end
	
	def self.hide(arel, usr, table_name = nil)
		kind = table_name || arel.name.tableize
		return arel unless usr
		arel1 = arel.where("NOT EXISTS (SELECT * FROM user_hidings WHERE #{kind}.user_id = user_hidings.hidden_user_id AND user_hidings.hiding_user_id = ?)", usr.id)
		if kind == "posts"
			# Check tags too
			return arel1.where("NOT EXISTS (SELECT * FROM tag_hidings WHERE tag_hidings.tag_id IN (SELECT id FROM tags LEFT JOIN posts_tags ON posts_tags.tag_id = tags.id WHERE posts_tags.post_id = posts.id) AND tag_hidings.user_id = ?)", usr.id)
		else
			return arel1
		end
	end
	
	def self.bot_socket
		@bot_socket
	end
	
	def self.bot_socket=(v)
		@bot_socket=v
	end
end

require 'spec_helper'

describe Post do
  it "should be locatable by postid" do
    post = Fabricate :post
    Post.by_postid(post.postid_text).should == post
  end

  it "should add/del tags" do
    post = Fabricate(:post, tags: [])
    tag_text = Faker::Lorem.word
    post.add_tag tag_text
    post.tags.first.tag_text.should == tag_text
    post.del_tag tag_text
    post.tags.should be_empty
  end

  it "should be safe from unwilling eyes" do
    post = Fabricate(:post)
    ignoring_user = Fabricate(:user)
    ignoring_user.hidden_users << post.user
    post.hidden_by?(ignoring_user).should == true
  end

  it "should be safe from unwanted eyes" do
    post = Fabricate(:post)
    other_user = Fabricate(:user)
    post.accessible_by?(other_user).should == true
    post.private = true
    post.accessible_by?(other_user).should == false
    post.accessible_by?(post.user).should == true
    post.target_user = other_user
    post.accessible_by?(other_user).should == true
  end
end
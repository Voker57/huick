require 'spec_helper'

describe PostsController do
  it "should display post" do
    user = Fabricate(:user)
    post = Fabricate(:post, :user => user, :private => true)
    get "post", postid: post.postid_text
    @response.code.should == '200'
  end

  it "should display recent posts" do
    5.times { Fabricate :post }
    get "recent"
    @response.code.should == '200'
  end
end
Fabricator :user do
  username { Faker::Lorem.word + Fabricate.sequence.to_s }
  jid { Faker::Internet.email }
  description { Faker::Lorem.paragraph }
  email { Faker::Internet.email }
  full_name { Faker::Lorem.words }
  posts(count: 10) { Fabricate :post, user: nil }
end
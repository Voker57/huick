Fabricator :tag do
  tag_text { Faker::Lorem.word + Fabricate.sequence.to_s}
end
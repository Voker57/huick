Fabricator :post do
  user { Fabricate(:user) }
  resource { Faker::Lorem.sentence }
  post_content { Faker::Lorem.paragraphs }
  tags {|post| (0..10).map{ Fabricate(:tag) } }
end
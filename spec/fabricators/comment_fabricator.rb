Fabricator :comment do
  user { Fabricate :user }
  comment_text { Faker::Lorem.paragraphs }
  resource { Faker::Lorem.sentence }
end
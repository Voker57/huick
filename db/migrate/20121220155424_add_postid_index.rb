class AddPostidIndex < ActiveRecord::Migration[4.2]
	def up
		add_index :posts, :postid, :unique => true
	end

	def down
		remove_index :posts, :postid
	end
end

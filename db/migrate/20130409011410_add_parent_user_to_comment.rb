class AddParentUserToComment < ActiveRecord::Migration[4.2]
	def change
		add_column :comments, :parent_comment_user_id, :integer
	end
end

class CreateUserBlacklists < ActiveRecord::Migration[4.2]
	def change
		create_table :user_blacklists do |t|
			t.references :blacklister
			t.references :blacklisted

			t.timestamps
		end
		add_index :user_blacklists, :blacklister_id
		add_index :user_blacklists, :blacklisted_id
	end
end

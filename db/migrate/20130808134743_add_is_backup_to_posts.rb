class AddIsBackupToPosts < ActiveRecord::Migration[4.2]
  def change
    add_column :posts, :is_backup, :boolean, :default => false
  end
end

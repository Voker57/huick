class AddOffedToUsers < ActiveRecord::Migration[4.2]
	def change
		add_column :users, :offed, :boolean, :default => false
		add_index :users, :offed
	end
end

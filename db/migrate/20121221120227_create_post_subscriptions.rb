class CreatePostSubscriptions < ActiveRecord::Migration[4.2]
	def change
		create_table :post_subscriptions do |t|
			t.references :post
			t.references :user
		end
	end
end

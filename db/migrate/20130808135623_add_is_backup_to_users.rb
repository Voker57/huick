class AddIsBackupToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :is_backup, :boolean, :default => false
  end
end

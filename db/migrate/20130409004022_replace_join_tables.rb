class ReplaceJoinTables < ActiveRecord::Migration[4.2]
	def change
		drop_table :comment_acknowledgements
		create_table :comment_acknowledgements, id: false do |t|
			t.references :comment
			t.references :user
		end
		add_index :comment_acknowledgements, [:comment_id, :user_id], unique: true
		
		drop_table :post_acknowledgements
		create_table :post_acknowledgements, id: false do |t|
			t.references :post
			t.references :user
		end
		add_index :post_acknowledgements, [:post_id, :user_id], unique: true
		
		drop_table :post_subscriptions
		create_table :post_subscriptions, id: false do |t|
			t.references :post
			t.references :user
		end
		add_index :post_subscriptions, [:post_id, :user_id], unique: true
		
		drop_table :tag_hidings
		create_table :tag_hidings, id: false do |t|
			t.references :tag
			t.references :user
		end
		add_index :tag_hidings, [:tag_id, :user_id], unique: true
		
		drop_table :tag_subscriptions
		create_table :tag_subscriptions, id: false do |t|
			t.references :tag
			t.references :user
		end
		add_index :tag_subscriptions, [:tag_id, :user_id], unique: true
		
		drop_table :user_blacklists
		create_table :user_blacklists, id: false do |t|
			t.references :blacklister
			t.references :blacklisted
		end
		add_index :user_blacklists, [:blacklister_id, :blacklisted_id], unique: true
		
		drop_table :user_hidings
		create_table :user_hidings, id: false do |t|
			t.references :hiding_user
			t.references :hidden_user
		end
		add_index :user_hidings, [:hiding_user_id, :hidden_user_id], unique: true
		
		drop_table :user_subscriptions
		create_table :user_subscriptions, id: false do |t|
			t.references :subscriber
			t.references :subscribed
		end
		add_index :user_subscriptions, [:subscriber_id, :subscribed_id], unique: true
	end
end

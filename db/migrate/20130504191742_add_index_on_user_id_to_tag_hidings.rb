class AddIndexOnUserIdToTagHidings < ActiveRecord::Migration[4.2]
	def change
		add_index :tag_hidings, :user_id
	end
end

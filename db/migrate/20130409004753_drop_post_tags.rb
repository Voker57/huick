class DropPostTags < ActiveRecord::Migration[4.2]
	def change
		drop_table :post_tags
		remove_column :posts, :post_tag_id
	end
end

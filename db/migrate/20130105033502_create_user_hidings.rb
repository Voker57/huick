class CreateUserHidings < ActiveRecord::Migration[4.2]
	def change
		create_table :user_hidings do |t|
			t.references :hiding_user
			t.references :hidden_user

			t.timestamps
		end
		add_index :user_hidings, :hiding_user_id
		add_index :user_hidings, :hidden_user_id
	end
end

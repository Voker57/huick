class CreateUserSubscriptions < ActiveRecord::Migration[4.2]
	def change
		create_table :user_subscriptions do |t|
			t.references :subscriber
			t.references :subscribed
		end
	end
end

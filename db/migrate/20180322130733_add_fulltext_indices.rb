class AddFulltextIndices < ActiveRecord::Migration[5.1]
  def change
	add_index :comments, "setweight(to_tsvector('english', coalesce(comment_text, '')), 'A')", using: "rum", order: {name: "rum_tsvector_ops"}, name: :comment_text_index_en
	add_index :comments, "setweight(to_tsvector('russian', coalesce(comment_text, '')), 'A')", using: "rum", order: {name: "rum_tsvector_ops"}, name: :comment_text_index_ru
	add_index :posts, "setweight(to_tsvector('english', coalesce(post_content, '')), 'A')", using: "rum", order: {name: "rum_tsvector_ops"}, name: :post_text_index_en
	add_index :posts, "setweight(to_tsvector('russian', coalesce(post_content, '')), 'A')", using: "rum", order: {name: "rum_tsvector_ops"}, name: :post_text_index_ru
  end
end

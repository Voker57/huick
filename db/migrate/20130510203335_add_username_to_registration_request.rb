class AddUsernameToRegistrationRequest < ActiveRecord::Migration[4.2]
  def change
    add_column :registration_requests, :username, :string
    add_index :registration_requests, :username
  end
end

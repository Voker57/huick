class AddIndicesOnNotified < ActiveRecord::Migration[4.2]
	def change
		add_index :posts, :notified
		add_index :user_subscriptions, :notified
		add_index :post_recommendations, :notified
		add_index :comments, :notified
		add_index :comment_recommendations, :notified
	end
end
class AddDefaultValueToParentCommentNumber < ActiveRecord::Migration[4.2]
	def change
		remove_column :comments, :parent_comment_number
		add_column :comments, :parent_comment_number, :integer, default: 0
	end
end

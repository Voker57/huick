class ChangePostidToInteger < ActiveRecord::Migration[4.2]
	def up
		remove_column :posts, :postid
		add_column :posts, :postid, :integer
	end

	def down
		remove_column :posts, :postid
		add_column :posts, :postid, :string
	end
end

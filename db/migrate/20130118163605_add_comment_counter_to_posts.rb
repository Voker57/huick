class AddCommentCounterToPosts < ActiveRecord::Migration[4.2]
	def up
		add_column :posts, :comment_counter, :integer, :default => 1
		Post.reset_column_information
		say_with_time "Guessing comment counters" do
			Post.all.each do |pst|
				if pst.comments.last
					pst.comment_counter = pst.comments.last.number + 1
					pst.save!
				end
			end
		end
	end
	
	def down
		remove_column :posts, :comment_counter
	end
end

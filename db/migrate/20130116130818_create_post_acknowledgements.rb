class CreatePostAcknowledgements < ActiveRecord::Migration[4.2]
	def change
		create_table :post_acknowledgements do |t|
			t.references :post
			t.references :user

			t.timestamps
		end
		add_index :post_acknowledgements, :post_id
		add_index :post_acknowledgements, :user_id
	end
end

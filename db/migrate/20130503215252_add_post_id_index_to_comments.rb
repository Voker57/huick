class AddPostIdIndexToComments < ActiveRecord::Migration[4.2]
	def change
		add_index :comments, :post_id
	end
end

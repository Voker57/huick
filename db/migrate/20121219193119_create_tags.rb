class CreateTags < ActiveRecord::Migration[4.2]
	def change
		create_table :tags do |t|
			t.string :tag_text
			
			t.timestamps
		end
		add_index :tags, :tag_text
	end
end

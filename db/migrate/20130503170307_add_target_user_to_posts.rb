class AddTargetUserToPosts < ActiveRecord::Migration[4.2]
	def change
		add_column :posts, :target_user_id, :integer
		add_index :posts, :target_user_id
	end
end

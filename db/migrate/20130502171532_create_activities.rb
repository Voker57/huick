class CreateActivities < ActiveRecord::Migration[4.2]
	def change
		create_table :activities do |t|
			t.references :post
			t.references :comment_recommendation
			t.references :post_recommendation
			t.datetime :timestamp
			
			t.timestamps
		end
		add_index :activities, :post_id
		add_index :activities, :comment_recommendation_id
		add_index :activities, :post_recommendation_id
		add_index :activities, :timestamp
	end
end

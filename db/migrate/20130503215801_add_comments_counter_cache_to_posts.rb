class AddCommentsCounterCacheToPosts < ActiveRecord::Migration[4.2]
	def up
		add_column :posts, :comments_count, :integer, null: false, default: 0
		Post.find_each do |pst|
			Post.reset_counters(pst.id, :comments)
		end
	end
	
	def down
		remove_column :posts, :comments_count
	end
end

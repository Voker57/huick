class ReplaceParentCommentWithNumber < ActiveRecord::Migration[4.2]
	def change
		remove_column :comments, :parent_comment_id
		add_column :comments, :parent_comment_number, :integer
	end
end

class CreatePosts < ActiveRecord::Migration[4.2]
	def change
		create_table :posts do |t|
			t.references :user
			t.text :post_content
			t.references :post_tag
			t.string :resource
			t.string :postid
			
			t.timestamps
		end
		add_index :posts, :postid
	end
end

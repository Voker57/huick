class CreatePostTags < ActiveRecord::Migration[4.2]
	def change
		create_table :post_tags do |t|
			t.references :post
			t.references :tag
		end
	end
end

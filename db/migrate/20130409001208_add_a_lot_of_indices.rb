class AddALotOfIndices < ActiveRecord::Migration[4.2]
	def change
		add_index :post_subscriptions, [:post_id, :user_id], unique: true
		add_index :post_tags, [:post_id, :tag_id], unique: true
		
		remove_index :tag_hidings, :tag_id
		remove_index :tag_hidings, :user_id
		add_index :tag_hidings, [:user_id, :tag_id], unique: true
		
		remove_index :tag_subscriptions, :tag_id
		remove_index :tag_subscriptions, :user_id
		add_index :tag_subscriptions, [:user_id, :tag_id], unique: true
		
		remove_index :tags, :tag_text
		add_index :tags, :tag_text, unique: true
		
		remove_index :user_blacklists, :blacklisted_id
		remove_index :user_blacklists, :blacklister_id
		add_index :user_blacklists, [:blacklisted_id, :blacklister_id], unique: true
		
		remove_index :user_hidings, :hidden_user_id
		remove_index :user_hidings, :hiding_user_id
		add_index :user_hidings, [:hidden_user_id, :hiding_user_id], unique: true
		
		add_index :user_subscriptions, [:subscriber_id, :subscribed_id], unique: true
		
		add_index :users, :username, unique: true
		add_index :users, :jid, unique: true
	end
end

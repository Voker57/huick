class AddIndexOnSubscriberId < ActiveRecord::Migration[4.2]
	def change
		add_index :user_subscriptions, :subscribed_id
	end
end

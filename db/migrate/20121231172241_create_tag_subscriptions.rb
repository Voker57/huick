class CreateTagSubscriptions < ActiveRecord::Migration[4.2]
	def change
		create_table :tag_subscriptions do |t|
			t.references :tag
			t.references :user

			t.timestamps
		end
		add_index :tag_subscriptions, :tag_id
		add_index :tag_subscriptions, :user_id
	end
end

class CreateCommentRecommendations < ActiveRecord::Migration[4.2]
	def change
		create_table :comment_recommendations do |t|
			t.references :comment
			t.references :user
			t.text :text
			t.boolean :notified, :default => false

			t.timestamps
		end
		add_index :comment_recommendations, :comment_id
		add_index :comment_recommendations, :user_id
	end
end

class CreateCommentAcknowledgements < ActiveRecord::Migration[4.2]
	def change
		create_table :comment_acknowledgements do |t|
			t.references :comment
			t.references :user

			t.timestamps
		end
		add_index :comment_acknowledgements, :comment_id
		add_index :comment_acknowledgements, :user_id
	end
end

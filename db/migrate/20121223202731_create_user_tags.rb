class CreateUserTags < ActiveRecord::Migration[4.2]
	def change
		create_table :user_tags do |t|
			t.references :post
			t.references :tag
		end
	end
end

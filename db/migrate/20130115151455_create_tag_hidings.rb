class CreateTagHidings < ActiveRecord::Migration[4.2]
	def change
		create_table :tag_hidings do |t|
			t.references :user
			t.references :tag

			t.timestamps
		end
		add_index :tag_hidings, :user_id
		add_index :tag_hidings, :tag_id
	end
end

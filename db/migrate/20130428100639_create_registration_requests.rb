class CreateRegistrationRequests < ActiveRecord::Migration[4.2]
	def change
		create_table :registration_requests do |t|
			t.string :jid

			t.timestamps
		end
		add_index :registration_requests, :jid, unique: true
	end
end

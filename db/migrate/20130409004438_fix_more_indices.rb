class FixMoreIndices < ActiveRecord::Migration[4.2]
	def change
		 remove_index "comments", ["number", "post_id"]
		 add_index "comments", ["number", "post_id"], unique: true
		 
		 add_index :posts, :user_id
	end
end

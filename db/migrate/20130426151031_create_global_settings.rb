class CreateGlobalSettings < ActiveRecord::Migration[4.2]
	def change
		create_table :global_settings do |t|
			t.string :key
			t.string :value
			
			t.timestamps
		end
		add_index :global_settings, :key, :unique => true
	end
end

class ReplaceRecommendationTextWithComment < ActiveRecord::Migration[4.2]
	def change
		change_table :post_recommendations do |t|
			t.remove :text
			t.references :recommending_comment
		end
		
		change_table :comment_recommendations do |t|
			t.remove :text
			t.references :recommending_comment
		end
	end
end

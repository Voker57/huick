class CreatePostsTags < ActiveRecord::Migration[4.2]
	def change
		create_table :posts_tags, id: false do |t|
			t.references :post
			t.references :tag
		end
		add_index :posts_tags, [:post_id, :tag_id]
		
		drop_table :user_tags
	end
end

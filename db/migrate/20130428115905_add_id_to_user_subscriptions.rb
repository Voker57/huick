class AddIdToUserSubscriptions < ActiveRecord::Migration[4.2]
	def change
		add_column :user_subscriptions, :id, :primary_key
	end
end

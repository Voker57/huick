class CreateComments < ActiveRecord::Migration[4.2]
	def change
		create_table :comments do |t|
			t.text :comment_text
			t.references :user
			t.references :parent_comment
			t.references :post
			t.string :resource
			t.integer :number
			
			
			t.timestamps
		end
		add_index :comments, [:number, :post_id]
	end
end

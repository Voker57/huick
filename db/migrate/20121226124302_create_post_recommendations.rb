class CreatePostRecommendations < ActiveRecord::Migration[4.2]
	def change
		create_table :post_recommendations do |t|
			t.references :post
			t.references :user
			t.text :text
			t.boolean :notified, :default => false

			t.timestamps
		end
		add_index :post_recommendations, :post_id
		add_index :post_recommendations, :user_id
	end
end

class AddNotifiedToPostsAndComments < ActiveRecord::Migration[4.2]
	def change
		change_table :posts do |t|
			t.column :notified, :boolean, :default => false
		end
		change_table :comments do |t|
			t.column :notified, :boolean, :default => false
		end
	end
end

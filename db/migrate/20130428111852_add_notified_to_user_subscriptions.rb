class AddNotifiedToUserSubscriptions < ActiveRecord::Migration[4.2]
	def change
		add_column :user_subscriptions, :notified, :boolean, :default => false
	end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180322130733) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "rum"

  create_table "activities", id: :serial, force: :cascade do |t|
    t.integer "post_id"
    t.integer "comment_recommendation_id"
    t.integer "post_recommendation_id"
    t.datetime "timestamp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["comment_recommendation_id"], name: "index_activities_on_comment_recommendation_id"
    t.index ["post_id"], name: "index_activities_on_post_id"
    t.index ["post_recommendation_id"], name: "index_activities_on_post_recommendation_id"
    t.index ["timestamp"], name: "index_activities_on_timestamp"
  end

  create_table "comment_acknowledgements", id: false, force: :cascade do |t|
    t.integer "comment_id"
    t.integer "user_id"
    t.index ["comment_id", "user_id"], name: "index_comment_acknowledgements_on_comment_id_and_user_id", unique: true
  end

  create_table "comment_recommendations", id: :serial, force: :cascade do |t|
    t.integer "comment_id"
    t.integer "user_id"
    t.boolean "notified", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "recommending_comment_id"
    t.index ["comment_id"], name: "index_comment_recommendations_on_comment_id"
    t.index ["notified"], name: "index_comment_recommendations_on_notified"
    t.index ["recommending_comment_id"], name: "index_comment_recommendations_on_recommending_comment_id"
    t.index ["user_id"], name: "index_comment_recommendations_on_user_id"
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.text "comment_text"
    t.integer "user_id"
    t.integer "post_id"
    t.string "resource"
    t.integer "number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "notified", default: false
    t.integer "parent_comment_user_id"
    t.integer "parent_comment_number", default: 0
    t.index "setweight(to_tsvector('english'::regconfig, COALESCE(comment_text, ''::text)), 'A'::\"char\")", name: "comment_text_index_en", using: :rum
    t.index "setweight(to_tsvector('russian'::regconfig, COALESCE(comment_text, ''::text)), 'A'::\"char\")", name: "comment_text_index_ru", using: :rum
    t.index ["notified"], name: "index_comments_on_notified"
    t.index ["number", "post_id"], name: "index_comments_on_number_and_post_id", unique: true
    t.index ["post_id"], name: "index_comments_on_post_id"
  end

  create_table "global_settings", id: :serial, force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["key"], name: "index_global_settings_on_key", unique: true
  end

  create_table "post_acknowledgements", id: false, force: :cascade do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.index ["post_id", "user_id"], name: "index_post_acknowledgements_on_post_id_and_user_id", unique: true
  end

  create_table "post_recommendations", id: :serial, force: :cascade do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.boolean "notified", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "recommending_comment_id"
    t.index ["notified"], name: "index_post_recommendations_on_notified"
    t.index ["post_id"], name: "index_post_recommendations_on_post_id"
    t.index ["recommending_comment_id"], name: "index_post_recommendations_on_recommending_comment_id"
    t.index ["user_id"], name: "index_post_recommendations_on_user_id"
  end

  create_table "post_subscriptions", id: false, force: :cascade do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.index ["post_id", "user_id"], name: "index_post_subscriptions_on_post_id_and_user_id", unique: true
  end

  create_table "posts", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.text "post_content"
    t.string "resource"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "postid"
    t.boolean "notified", default: false
    t.integer "comment_counter", default: 1
    t.integer "target_user_id"
    t.integer "comments_count", default: 0, null: false
    t.boolean "private", default: false
    t.boolean "is_backup", default: false
    t.index "setweight(to_tsvector('english'::regconfig, COALESCE(post_content, ''::text)), 'A'::\"char\")", name: "post_text_index_en", using: :rum
    t.index "setweight(to_tsvector('russian'::regconfig, COALESCE(post_content, ''::text)), 'A'::\"char\")", name: "post_text_index_ru", using: :rum
    t.index ["created_at"], name: "index_posts_on_created_at"
    t.index ["notified"], name: "index_posts_on_notified"
    t.index ["postid"], name: "index_posts_on_postid", unique: true
    t.index ["private"], name: "index_posts_on_private"
    t.index ["target_user_id"], name: "index_posts_on_target_user_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "posts_tags", id: false, force: :cascade do |t|
    t.integer "post_id"
    t.integer "tag_id"
    t.index ["post_id", "tag_id"], name: "index_posts_tags_on_post_id_and_tag_id"
  end

  create_table "registration_requests", id: :serial, force: :cascade do |t|
    t.string "jid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "username"
    t.index ["jid"], name: "index_registration_requests_on_jid", unique: true
    t.index ["username"], name: "index_registration_requests_on_username"
  end

  create_table "tag_hidings", id: false, force: :cascade do |t|
    t.integer "tag_id"
    t.integer "user_id"
    t.index ["tag_id", "user_id"], name: "index_tag_hidings_on_tag_id_and_user_id", unique: true
    t.index ["user_id"], name: "index_tag_hidings_on_user_id"
  end

  create_table "tag_subscriptions", id: false, force: :cascade do |t|
    t.integer "tag_id"
    t.integer "user_id"
    t.index ["tag_id", "user_id"], name: "index_tag_subscriptions_on_tag_id_and_user_id", unique: true
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "tag_text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["tag_text"], name: "index_tags_on_tag_text", unique: true
  end

  create_table "user_blacklists", id: false, force: :cascade do |t|
    t.integer "blacklister_id"
    t.integer "blacklisted_id"
    t.index ["blacklister_id", "blacklisted_id"], name: "index_user_blacklists_on_blacklister_id_and_blacklisted_id", unique: true
  end

  create_table "user_hidings", id: false, force: :cascade do |t|
    t.integer "hiding_user_id"
    t.integer "hidden_user_id"
    t.index ["hidden_user_id"], name: "index_user_hidings_on_hidden_user_id"
    t.index ["hiding_user_id", "hidden_user_id"], name: "index_user_hidings_on_hiding_user_id_and_hidden_user_id", unique: true
  end

  create_table "user_subscriptions", force: :cascade do |t|
    t.integer "subscriber_id"
    t.integer "subscribed_id"
    t.boolean "notified", default: false
    t.index ["notified"], name: "index_user_subscriptions_on_notified"
    t.index ["subscribed_id"], name: "index_user_subscriptions_on_subscribed_id"
    t.index ["subscriber_id", "subscribed_id"], name: "index_user_subscriptions_on_subscriber_id_and_subscribed_id", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "username"
    t.string "jid"
    t.string "access_token"
    t.boolean "banned", default: false
    t.string "description"
    t.string "email"
    t.string "full_name"
    t.boolean "offed", default: false
    t.boolean "is_backup", default: false
    t.index ["access_token"], name: "index_users_on_access_token", unique: true
    t.index ["jid"], name: "index_users_on_jid", unique: true
    t.index ["offed"], name: "index_users_on_offed"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
